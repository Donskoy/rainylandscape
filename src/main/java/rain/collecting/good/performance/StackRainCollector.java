package rain.collecting.good.performance;

import rain.collecting.LandscapeValidator;

import java.util.Stack;

/**
 * @author dmitry
 */
public class StackRainCollector {

    public static long calculateWaterAmount(int[] landscape) {

        if (!LandscapeValidator.isValid(landscape)) {
            System.err.println("Landscape is not valid");
            return -1;
        }

        Stack<Integer> peaks = new Stack<>();
        long sum = 0;
        Step step;
        Step prevStep = Step.START;
        Step nextStep;

        for (int i = 0; i < landscape.length; i++) {
            step = defineStep(landscape, i, prevStep);
            if (i <= landscape.length - 2) {
                nextStep = defineStep(landscape, i + 1, step);
            } else {
                nextStep = Step.FLAT;
            }

            switch (step) {
                case ASC:
                    if (prevStep != Step.START) {
                        if (peaks.isEmpty()) {
                            break;
                        }
                        Integer lastPeakIndex = peaks.peek();
                        int diff;
                        if (landscape[lastPeakIndex] > landscape[i]) {
                            diff = landscape[i] - landscape[i - 1];
                        } else {
                            diff = landscape[lastPeakIndex] - landscape[i - 1];
                        }

                        while (!peaks.isEmpty() && landscape[i] >= landscape[lastPeakIndex]) {
                            sum += (diff * (i - lastPeakIndex - 1));
                            landscape[i - 1] = landscape[lastPeakIndex];
                            peaks.pop();
                            if (peaks.isEmpty()) {
                                break;
                            }
                            diff = landscape[peaks.peek()] - landscape[lastPeakIndex];
                            lastPeakIndex = peaks.peek();
                        }

                        if (landscape[lastPeakIndex] > landscape[i]) {
                            diff = landscape[i] - landscape[i - 1];
                            sum += (diff * (i - lastPeakIndex - 1));
                        }
                    }
                    break;
            }

            if (nextStep == Step.DESC) {
                peaks.push(i);
            }

            prevStep = step;
        }

        return sum;
    }

    private static Step defineStep(int[] landscape, int index, Step previousStep) {
        if (previousStep == Step.START) {
            if (landscape[index] == 0) {
                return Step.START;
            } else if (landscape[index] <= landscape[index + 1]) {
                return Step.ASC;
            } else if (landscape[index] > landscape[index + 1]) {
                return Step.DESC;
            }
        }

        int previous = landscape[index - 1];
        int current = landscape[index];

        if (previous > current) {
            return Step.DESC;
        }
        if (previous < current) {
            return Step.ASC;
        }

        return Step.FLAT;
    }

    private enum Step {
        DESC,
        ASC,
        FLAT,
        START,
    }

}
