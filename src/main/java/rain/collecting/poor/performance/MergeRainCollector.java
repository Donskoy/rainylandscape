package rain.collecting.poor.performance;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author dmitry
 */
public class MergeRainCollector {

    private int[] landscape;
    private boolean biDirectional; // Useful for cases with gentle left slope and steep right one

    public MergeRainCollector() {
        this(false);
    }

    public MergeRainCollector(boolean biDirectional) {
        this.biDirectional = biDirectional;
    }

    public long calculateWaterAmount(int[] landscape) {
        this.landscape = landscape;
        long overall = 0;
        long diffSum;
        do {
            diffSum = calculateMergedSum();
            overall += diffSum;
            if (biDirectional) {
                ArrayUtils.reverse(this.landscape);
            }
        } while (diffSum != 0);
        return overall;
    }

    private long calculateMergedSum() {
        long sum = 0;
        int lastPeak = -1;
        int lastPeakIndex = -1;
        Step step;
        Step prevStep = Step.START;
        boolean merged;
        for (int i = 0; i < landscape.length; i++) {
            merged = false;
                step = defineStep(landscape, i, prevStep);
                switch (step) {
                    case ASC:
                        if (prevStep == Step.START) {
                            merged = true;
                        }
                        if (prevStep == Step.ASC || prevStep == Step.DESC || prevStep == Step.FLAT) {
                            if (landscape[i] >= lastPeak) {
                                int diff = lastPeak - landscape[i-1];
                                if (diff < 0) {
                                    break;
                                }
                                for (int j = lastPeakIndex + 1; j < i; j++) {
                                    sum += diff;
                                    landscape[j] += diff;
                                }
                                merged = true;
                            } else {
                                int diff = landscape[i] - landscape[i-1];
                                if (diff < 0) {
                                    break;
                                }
                                for (int j = lastPeakIndex + 1; j < i; j++) {
                                    sum += diff;
                                    landscape[j] += diff;
                                }
                            }
                        }
                        break;

                    case DESC:
                        if (i == 0) {
                            lastPeak = landscape[i];
                            lastPeakIndex = i;
                        }
                        else {
                            lastPeak = landscape[i-1];
                            lastPeakIndex = i-1;
                        }
                        break;

                    case START:
                        break;

                    case FLAT:
                        break;
                }

                if (merged) {
                    prevStep = Step.START;
                } else {
                    prevStep = step;
                }
        }

        return sum;
    }

    private Step defineStep(int[] landscape, int index, Step previousStep) {
        if (index == 0) {
            if (landscape[index] > 0) {
                return Step.DESC;
            }
            else {
                return Step.START;
            }
        }

        if (previousStep == Step.START) {
            if (landscape[index] == 0) {
                return Step.START;
            } else {
                return Step.DESC;
            }
        }

        int previous = landscape[index - 1];
        int current = landscape[index];

        if (previous > current) {
            return Step.DESC;
        }
        if (previous < current) {
            return Step.ASC;
        }

        return Step.FLAT;
    }

    private enum Step {
        DESC,
        ASC,
        FLAT,
        START,
    }

}
