package rain.collecting;

import rain.collecting.good.performance.StackRainCollector;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dmitry
 */
public class Benchmark {

    private static final int TEST_SIZE = 10000;

    public static void main(String[] args) {

        List<int[]> landscapes = new ArrayList<>();
        for (int i = 0; i < TEST_SIZE; i++) {
            landscapes.add(LandscapeGenerator.generateFullSize());
        }

        long startTime = System.currentTimeMillis();

        for (int[] landscape : landscapes) {
            StackRainCollector.calculateWaterAmount(landscape);
        }

        long stopTime = System.currentTimeMillis();

        double avg = (double) (stopTime - startTime) / TEST_SIZE;

        System.out.println("Millis per landscape: " + avg);

    }

}
