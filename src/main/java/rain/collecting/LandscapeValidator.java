package rain.collecting;

/**
 * @author dmitry
 */
public class LandscapeValidator {

    public static final int LENGTH_LIMIT = 32_000;
    public static final int HEIGHT_LIMIT = 32_000;

    public static boolean isValid(int[] landscape) {
        if (landscape.length > LENGTH_LIMIT || landscape.length == 0) {
            return false;
        }
        for (int height : landscape) {
            if (height > HEIGHT_LIMIT || height < 0) {
                return false;
            }
        }
        return true;
    }

}
