package rain.collecting;

import java.util.Random;

/**
 * @author dmitry
 */
public class LandscapeGenerator {

    public static final int LENGTH_LIMIT = 32_000;
    public static final int HEIGHT_LIMIT = 32_000;

    public static int[] generate(int size) {
        Random rnd = new Random();
        int[] landscape = new int[size];
        for (int i = 0; i < size; i++) {
            landscape[i] = rnd.nextInt(HEIGHT_LIMIT + 1);
        }
        return landscape;
    }

    public static int[] generateFullSize() {
        return generate(LENGTH_LIMIT);
    }

}
