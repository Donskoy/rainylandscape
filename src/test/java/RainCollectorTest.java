import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import rain.collecting.LandscapeValidator;
import rain.collecting.good.performance.StackRainCollector;

import java.util.*;

/**
 * @author dmitry
 */
public class RainCollectorTest {

    private Map<int[], Long> testData = new LinkedHashMap<>();
    private List<int[]> invalidLandscapes = new ArrayList<>();

    @Before
    public void init() {
        testData.put(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0}, 0L);
        testData.put(new int[] {4, 4, 4, 4, 4, 4, 4, 4, 4}, 0L);
        testData.put(new int[] {5, 2, 3, 4, 5, 4, 0, 3, 1}, 9L);
        testData.put(new int[] {0, 0, 1, 3, 0, 2, 3, 1, 0, 0}, 4L);
        testData.put(new int[] {0, 0, 3, 3, 0, 2, 3, 1, 0, 0}, 4L);
        testData.put(new int[] {0, 3, 3, 3, 0, 2, 3, 1, 0, 0}, 4L);
        testData.put(new int[] {3, 3, 3, 3, 0, 2, 3, 1, 0, 0}, 4L);
        testData.put(new int[] {6, 4, 0, 0, 3, 5, 8, 2, 3, 5}, 23L);
        testData.put(new int[] {7, 0, 2, 4, 3, 0, 0, 6, 8, 0, 2, 0, 4}, 44L);
        testData.put(new int[] {32000, 0, 0, 32000, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 64000L);
        testData.put(new int[] {32000, 0, 0, 30000, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 60000L);

        invalidLandscapes.add(new int[] {});
        invalidLandscapes.add(new int[] {32_001, 0, 1, 2});
        invalidLandscapes.add(new int[] {32_00, 0, 1, 2, -2});
    }

    @Test
    public void testRainCollector() {
        for (Map.Entry<int[], Long> entry : testData.entrySet()) {
            System.out.println(Arrays.toString(entry.getKey()));
            Assert.assertEquals("Failed to count water for landscape: " + Arrays.toString(entry.getKey()),
                    (long) entry.getValue(), StackRainCollector.calculateWaterAmount(entry.getKey()));
        }
    }

    @Test
    public void testValidator() {
        for (int[] landscape : invalidLandscapes) {
            Assert.assertFalse(
                    "Landscape is not valid: " + Arrays.toString(landscape),
                    LandscapeValidator.isValid(landscape));
        }
    }


}
